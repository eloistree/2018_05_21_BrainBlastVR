﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlueLife : MonoBehaviour {

    public float m_life = 100;
    public float m_lifeInitial=100;
    public float m_lostLifeBySecond = 40;
    public float m_regenerationBySecond = 5;
    public float m_affectedAfter = 2;

    public GlueToObject m_glueTo;

    public float m_timeNotGlued;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (m_life == 0)
            return;

        m_life += m_regenerationBySecond * Time.deltaTime;

        if (!m_glueTo.IsThereObjectAround())
            m_timeNotGlued += Time.deltaTime;
        else m_timeNotGlued = 0;

        if (m_timeNotGlued > m_affectedAfter)
        {
            if(m_life>0)
            m_life -= m_lostLifeBySecond * Time.deltaTime;

            if (m_life < 0)
                m_life = 0;
        }

        //TO ADD TO A NORTH SCRIPT
        transform.localScale = Vector3.one * (float)(m_life / m_lifeInitial);
	}
}
